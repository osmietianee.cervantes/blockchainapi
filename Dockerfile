FROM tiangolo/uvicorn-gunicorn:python3.9
WORKDIR /usr/src
COPY requirements.txt /usr/src/requirements.txt
COPY ./app /usr/src/app
RUN pip install --upgrade pip && pip install -r requirements.txt
